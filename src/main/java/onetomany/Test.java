package onetomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {

        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Department.class)
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();

        Department d1 = new Department("Finace", "This is the  finance department");
        Department d2 = new Department("production", "This is the  production department");

        Employee e1 = new Employee("F1", "L1", 3435d);
        Employee e2 = new Employee("F2", "L2", 3435d);
        Employee e3 = new Employee("F3", "L3", 3435d);
        Employee e4 = new Employee("F4", "L4", 3435d);
        Employee e5 = new Employee("F5", "L5", 3435d);

        e2.setDepartment(d1);
        e5.setDepartment(d1);
        e1.setDepartment(d2);
        e3.setDepartment(d2);
        e4.setDepartment(d2);

        session.persist(d1);
        session.persist(d2);

        session.persist(e1);
        session.persist(e2);
        session.persist(e3);
        session.persist(e4);
        session.persist(e5);

        transaction.commit();
    }
}
