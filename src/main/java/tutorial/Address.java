package tutorial;

import jakarta.persistence.Embeddable;

@Embeddable
public class Address {

    private String street;
    private String building;
    private int appNumber;

    public Address(String street, String building, int appNumber) {
        this.street = street;
        this.building = building;
        this.appNumber = appNumber;
    }

    public Address() {
    }
}
