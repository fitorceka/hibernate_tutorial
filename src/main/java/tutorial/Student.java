package tutorial;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "STUDENT_INFO")
public class Student {

    @EmbeddedId
    private StudentId studentId;


//    @Column(name = "FIRST_NAME", length = 100, nullable = false)
//    private String firstName;

    @Column(name = "LAST_NAME", length = 100, nullable = false)
    private String lastName;

    @Column(name = "AGE", nullable = false)
    private int age;

    @Column(name = "EMAIL", length = 100, nullable = false, unique = true)
    private String email;

    @Embedded
    private Address address;

    public Student() {
    }

    public Student(String lastName, int age, String email) {
        this.lastName = lastName;
        this.age = age;
        this.email = email;
    }

    public void setStudentId(StudentId studentId) {
        this.studentId = studentId;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    //    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Student{" +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }
}
