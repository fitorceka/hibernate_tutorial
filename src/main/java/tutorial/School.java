package tutorial;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;

@Entity
public class School extends BaseEntity {

    private String name;
    private int studentCount;

    @Embedded
    private Address address;

    public School() {
    }

    public School(String name, int studentCount) {
        this.name = name;
        this.studentCount = studentCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(int studentCount) {
        this.studentCount = studentCount;
    }
}
