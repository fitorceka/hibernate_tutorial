package tutorial;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class TestDB {

    public static void main(String[] args) {

        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();

        Student student = new Student("Fistek1", 22, "ff1@gmail.com");

        // per te ruajtur te dhena ne database
        session.persist(student);

        // per te gjetur te dhena
//        System.out.println("Before update");
//        Student found = session.find(Student.class, 1);
//        System.out.println(found);

        System.out.println("-------------------------------------");

        //per te updatuar te dhena
//        found.setAge(24);
//        session.merge(found);
//
//        System.out.println("After update");
//        found = session.find(Student.class, 1);
//        System.out.println(found);

        // per te fshire te dhena
//        session.remove(found);
        System.out.println("-------------------");


        System.out.println("Ater delete");
//
//        found = session.find(Student.class, 1);
//        System.out.println(found);



        transaction.commit();
    }
}
