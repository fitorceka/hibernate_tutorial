package tutorial;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class StudentId implements Serializable {

    private int number;
    private String firstName;

    public StudentId() {
    }

    public StudentId(int number, String firstName) {
        this.number = number;
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudentId studentId = (StudentId) o;

        if (number != studentId.number) return false;
        return firstName.equals(studentId.firstName);
    }

    @Override
    public int hashCode() {
        int result = number;
        result = 31 * result + firstName.hashCode();
        return result;
    }
}
