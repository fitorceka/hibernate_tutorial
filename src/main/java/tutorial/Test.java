package tutorial;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {


        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();

        StudentId id =new StudentId(1, "Filan1");
        Address address = new Address("Street", "Bu1", 20);
        Student student = new Student("Fisteku", 22, "ff2@gmail.com");
        student.setStudentId(id);
        student.setAddress(address);

        session.persist(student);

        transaction.commit();

    }
}
