package exercises.exercise2;

import exercises.exercise2.repo.GenreRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {
        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();

        GenreRepository gr = new GenreRepository(session);

        Genre g = new Genre("Action");
//        gr.saveGenre(g);

        Genre f =gr.findById(1);
        System.out.println(f);

        gr.removeGenre(f);
    }
}
