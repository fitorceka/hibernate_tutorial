package exercises.exercise2.repo;

import exercises.exercise2.Genre;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class GenreRepository {

    private final Session session;

    public GenreRepository(Session session) {
        this.session = session;
    }

    public void saveGenre(Genre genre) {
        Transaction t = session.beginTransaction();
        session.persist(genre);
        t.commit();
    }

    public void removeGenre(Genre genre) {
        Transaction t = session.beginTransaction();
        session.remove(genre);
        t.commit();
    }

    public Genre findByName(String name) {
        Transaction t = session.beginTransaction();
        String hql = "select G from Genre G where" +
                " G.name = :n";

        Genre found = session.createQuery(hql, Genre.class)
                        .setParameter("n", name)
                                .getSingleResult();
        t.commit();

        return found;
    }

    public Genre findById(int id) {
        Transaction t = session.beginTransaction();

        Genre found = session.find(Genre.class, id);
        t.commit();

        return found;
    }

    public List<Genre> findAll() {
        Transaction t = session.beginTransaction();
        String hql = "select G from Genre G";

        List<Genre> all = session.createQuery(hql, Genre.class)
                .getResultList();
        t.commit();

        return all;
    }
}
