package exercises.exercise1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {

        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();

        Customer customer = new Customer("Filan2 Fisteku2",
                "ff3@gmail.com", "1234567", "Dhaskal Todri");

//        session.persist(customer); kjo eshte per te bere save


        System.out.println("Find Before update");
        Customer found = session.find(Customer.class, 4);
        System.out.println(found);
        System.out.println();

        found.setAddress("Tirane");
        found.setPhoneNumber("7654321");
        session.merge(found);

        System.out.println("After update");
        found = session.find(Customer.class, 4);
        System.out.println(found);

        System.out.println("For delete");
        found = session.find(Customer.class, 3);
        session.remove(found);

        transaction.commit();
    }
}
