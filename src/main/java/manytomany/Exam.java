package manytomany;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;

import java.util.List;

@Entity
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int examId;

    private String subject;
    private int noOfQuestions;

    @ManyToMany
    @JoinTable(name = "EXAM_STUDENT",
               joinColumns = @JoinColumn(name = "EXAM_ID"),
               inverseJoinColumns = @JoinColumn(name = "STUDENT_ID"))
    private List<Student> students;

    public Exam() {
    }

    public Exam(String subject, int noOfQuestions) {
        this.subject = subject;
        this.noOfQuestions = noOfQuestions;
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(int noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
