package manytomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Test {

    public static void main(String[] args) {

        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Exam.class)
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();

        Student s1 = new Student("F1", "L1");
        Student s2 = new Student("F2", "L2");
        Student s3 = new Student("F3", "L3");


        Exam e1 = new Exam("Math", 20);
        Exam e2 = new Exam("Biology", 20);
        Exam e3 = new Exam("Tech", 20);

        e1.setStudents(List.of(s1, s2, s3));
        e2.setStudents(List.of(s3));
        e3.setStudents(List.of(s1, s3));

        session.persist(s1);
        session.persist(s2);
        session.persist(s3);

        session.persist(e1);
        session.persist(e2);
        session.persist(e3);

        transaction.commit();
    }
}
