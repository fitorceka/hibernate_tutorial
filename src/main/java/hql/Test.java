package hql;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {

        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();

        Scanner scanner = new Scanner(System.in);


        // find by id

//        String q = "select E from Employee E where E.id=2";
//        Query<Employee> query = session.createQuery(q, Employee.class);
//        Employee found = query.getSingleResult();
//        System.out.println(found);

        // find all

//        String hql = "select E from Employee E";
//        Query<Employee> query = session.createQuery(hql, Employee.class);
//        List<Employee> all = query.getResultList();
//        all.forEach(System.out::println);

        // perdor disa metoda

//        List<Employee> singleResult = query.setMaxResults(3).getResultList();
//        singleResult.forEach(System.out::println);

        // find by email
        // e parametrizuar

//        String hql = "select E from Employee E where E.email = :em";
//
//        System.out.print("Enter email to search: ");
//        String em = scanner.nextLine();
//
//        Query<Employee> query = session.createQuery(hql, Employee.class);
//        query.setParameter("em", em);
//        Employee found = query.getSingleResult();
//        System.out.println(found);

        // find between 2000 and 4000

//        String hql = "select E from Employee E where E.salary>=:lowBound " +
//                "and E.salary<=:upperBound";
//        String hql1 = "select E from Employee E where E.salary " +
//                "between 2000 and 4000";
//        Query<Employee> query = session.createQuery(hql, Employee.class);
//
//        System.out.print("Enter low bound: ");
//        double l = scanner.nextDouble();
//        System.out.print("Enter upper bound: ");
//        double u = scanner.nextDouble();
//
//        query.setParameter("lowBound", l);
//        query.setParameter("upperBound", u);
//
//        List<Employee> result = query.getResultList();
//        result.forEach(System.out::println);


        // order by

//        String hql = "select E from Employee E order by E.salary desc";
//        Query<Employee> query = session.createQuery(hql, Employee.class);
//        List<Employee> result = query.getResultList();
//        result.forEach(System.out::println);

        // find employees that contain name John

        String hql = "select E from Employee E where E.firstName like '%John%'";
        Query<Employee> query = session.createQuery(hql, Employee.class);
        List<Employee> result = query.getResultList();
        result.forEach(System.out::println);

        transaction.commit();

    }
}
