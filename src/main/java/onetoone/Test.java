package onetoone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {


        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();

        Users users = new Users("Filan", "Fisteku");
        UserDetails ud = new UserDetails(22, "ff@gmail.com", users);

        session.persist(users);
        session.persist(ud);

        transaction.commit();
    }
}
